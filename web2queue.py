'''
Created on Sep 14, 2016

The script is performs one of the following actions: send, recv, stats, clean,
check, abort, and stats. The configuration of the script is done via a json file.

The idea is to run the send, recv, check, clean, and stats actions regularly,
e.g. as a cron job. The abort action stops and resets all running jobs
(should be done only after the send cron job is stopped).

Prerequisites:
- The user that runs the script must be able to login to the remote host
  without password (ssh keys...). Note that a different user name may be used
  via an optional configuration variable (remoteuser).

Suggestions:
- in order to make sure that cron jobs do not interfere use flock

action send: sends new jobs from the local dir (where the web server deposits the job files)
  to the remote host and adds a call of the programm using the job data:
  * check local upload folder for new job files (local)
  * rename to wrk (local)
  * transfer with sftp (local -> remote)
  * mkdir of output dir (remote)
  * enqueue actual command using job data (remote)
  * zip and remove output dir (remote)

action recv: receive finished results from the remote host
  * check remote for results (zip exits, job is not in qstat job list)
  * transfer all files of related to the job (zip, output, error, and wrk file)
  * rename the wrk to log or err (of the error file contains anything)
  * unzip and change permissions to 777
  * move everything to subfolders (HASH... -> H/A/S/H...)

action abort
- note: make sure to stop the cron job first
- removes all jobs in the queue (including running)
- resets all jobs (.wrk -> job) locally
- removes remote data belonging to the jobs
- notifies of any remaining remote data

action clean
- remove all results (i.e. everything but the log file)
  that are older than a specified number of days (maxage variable)

action status
- get statistics (date, jobs processed and running / queued ...)


action check:
- checks if there are unusually many jobs in the queue
- checks if there are any jobs hanging in the queue

JSON input
- remote: the remote host with the queing system
- queueing: command to enqueue the job (needs to set the jobname to the hash.
        therefore it must contain {hsh}. also make sure that the .o and .e
        files are saved in CWD)
- ldir: the local directory where the webserver deposits .job files and
        where the result files should be saved
- rdir: the directory on the remote host where the job file and output can be
        stored temporarily
- command: the command line to be submitted to the queue. it needs to contain
        {ifile} and {odir} which will be substituted by the input file and
        output directory.
- subject: the subject line for the result mail
- url: the URL leading to a result when a job's hash is appended.
- mailserver the mailserver to use
- mailfrom sender mail adress to use fro sending mail
- mail: the text of a result mail
- errmail: the text of an error mail
- maxage: maximal allowed age in days of log files to keep (used for action clean)
- njobsthr: warning thresholds for the number of jobs
optional variables
- remoteuser username for ssh/sftp connections to remote (if not given the user
  that runs the script will be used)
- mailserveruser username for sending via mailserver
- mailserverpass password for sending via mailserver

the mail and errmail may contain {KEY} statements where KEY can be any key from the job JSON file.

@author: maze
'''
import datetime
import fnmatch
import glob
import json
import logging
import optparse
import os
import paramiko
import re
import shutil
import subprocess
import sys
import time
import traceback

from smtplib import SMTP
from email.MIMEText import MIMEText
from email.Header import Header
from email.Utils import parseaddr, formataddr


class ZIPError(Exception):
    pass


def change_permissions(path, perm):
    """
    """
    os.chmod(path, perm)

    for root, dirs, files in os.walk(path):
        for momo in dirs:
            change_permissions(os.path.join(root, momo), perm)
        for momo in files:
            os.chmod(os.path.join(root, momo), perm)


def remove_from_remote(client, conf, hsh):
    """
    remove all files hsh.* from the remote
    """
    cmd = "rm -r {rdir}/{hsh}*".format(rdir=conf["rdir"], hsh=hsh)
    stdin, stdout, stderr = client.exec_command(cmd)
    err = "".join(stderr.readlines())
    if len(err) > 0:
        sys.stderr.write("Could not remove remote files for %s.\nCommand: %s\nError: %s" % (hsh, cmd, err))


def send_email(server, sender, recipient, subject, body, user=None, passwd=None):
    """Send an email.

    All arguments should be Unicode strings (plain ASCII works as well).

    Only the real name part of sender and recipient addresses may contain
    non-ASCII characters.

    The email will be properly MIME encoded and delivered though SMTP to
    localhost port 25.  This is easy to change if you want something different.

    The charset of the email will be the first one out of US-ASCII, ISO-8859-1
    and UTF-8 that can represent all the characters occurring in the email.
    @param server mail server to use
    @param sender sender mail adress to use
    @param sender recepient mail adress
    @param subject subject of the mail
    @param body mail body
    @param user username for login to mailserver (None: no login required)
    @param passwd password for login
    """

    # Header class is smart enough to try US-ASCII, then the charset we
    # provide, then fall back to UTF-8.
    header_charset = 'ISO-8859-1'

    # We must choose the body charset manually
    for body_charset in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
        try:
            body.encode(body_charset)
        except UnicodeError:
            pass
        else:
            break

    # Split real name (which is optional) and email address parts
    sender_name, sender_addr = parseaddr(sender)
    recipient_name, recipient_addr = parseaddr(recipient)

    # We must always pass Unicode strings to Header, otherwise it will
    # use RFC 2047 encoding even on plain ASCII strings.
    sender_name = str(Header(unicode(sender_name), header_charset))
    recipient_name = str(Header(unicode(recipient_name), header_charset))

    # Make sure email addresses do not contain non-ASCII characters
    sender_addr = sender_addr.encode('ascii')
    recipient_addr = recipient_addr.encode('ascii')

    # Create the message ('plain' stands for Content-Type: text/plain)
    msg = MIMEText(body.encode(body_charset), 'plain', body_charset)
    msg['From'] = formataddr((sender_name, sender_addr))
    msg['To'] = formataddr((recipient_name, recipient_addr))
    msg['Subject'] = Header(unicode(subject), header_charset)

    # Send the message via SMTP to EMAILSERVER
    smtp = SMTP(host=server)

    if user is not None:
        smtp.starttls()
        smtp.login(user, passwd)
    smtp.sendmail(sender, recipient, msg.as_string())
    smtp.quit()


def result_mail(res, conf):
    """
    @param res result dict
    @param web2queue configuration
    """
    # Prepare e-mail
    if "error" in res:
        msg = conf["errmail"]
    else:
        msg = conf["mail"]

    msg = msg.format(**res)
    subject = conf["subject"].format(**res)

    try:
        send_email(conf["mailserver"], conf["mailfrom"], res["email"], subject, msg, conf.get("mailserveruser", None), conf.get("mailserverpass", None))
    except Exception, inst:
        logging.warning("email error: " + str(inst) + " " + res["hsh"])
        traceback.print_exc(file=sys.stderr)


if __name__ == '__main__':
    # get parameters
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option('-c', '--conf', action="store", type="str", default=None, help="json configuration file")
    parser.add_option('-a', '--action', action="store", type="str", default=None, help="send: send jobs to queue\nrecv: receive jobs from queue\nabort: abort all running jobs\nstats: get usage stats and print to stats.json in the ldir\nremove old data as specified in config")
    parser.add_option('-f', '--flat', action="store_true", default=False, help="save results in subdirs HASH instead of H/A/S/H (only affects recv)")
    parser.add_option('--debug', action="store_true", default=False, help="enable debug output")

    (args, argss) = parser.parse_args()

    if args.conf is None:
        parser.error("no configuration file given")
        sys.exit()

    if not os.path.isfile(args.conf):
        parser.error("configuration file %s does not exist" % args.conf)
        sys.exit()

    try:
        f = open(args.conf, "r")
        conf = json.load(f)
        f.close()
    except Exception:
        traceback.print_exc(file=sys.stderr)
        raise Exception("configuration file could not be parsed")

    for key in [u"njobsthr"]:
        if not isinstance(conf[key], int):
            parser.error("option %s is not int " % key)
            sys.exit(2)

    for key in [u"remote", u"ldir", u"rdir", u"queueing", u"command", u"subject", u"mailserver", u"mailfrom", u"mail", u"errmail"]:
        if not(key in conf and isinstance(conf[key], (str, unicode))):
            parser.error("option %s is missing in configuration" % key)
            sys.exit(2)

    for opt, c in [('{rdir}/{hsh}.wrk', 'command'), ('{rdir}/{hsh} ', 'command'), ('hsh', 'queueing')]:
        if len([m.start() for m in re.finditer(opt, conf[c])]) < 1:
            parser.error("%s needs to contain %s at least once" % (c, opt))
            sys.exit(2)

    if args.action not in ["send", "recv", "abort", "stats", "clean", "check"]:
        parser.error("unknown action {action} given".format(action=args.action))
        sys.exit()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
        logging.getLogger("paramiko").setLevel(logging.WARNING)

    # setup ssh and sftp connection
    logging.debug("connecting to %s" % conf["remote"])
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.load_system_host_keys()
    try:
        client.connect(str(conf["remote"]), username=conf.get("remoteuser", None))
    except Exception:
        logging.error("can't connect via ssh with ", str(conf["remote"]))
        sys.exit(1)
    ftp = client.open_sftp()
    ftp.chdir(conf["rdir"])
    logging.debug("connected")

    if args.action == "abort":
        # delete jobs
        cmd = "scancel -u mitosd"
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        err = "".join(stderr.readlines())
#         logging.debug("qstat\nstdout %s\nstderr%s\n" % (out, err))
        if len(err) > 0:
            raise Exception("Could not delete jobs\nCommand: %s\nError: %s" % (cmd, err))

        # delete remote data and reset local jobs
        available = glob.glob("{ldir}/*.wrk".format(ldir=conf["ldir"]))
        for wrk in available:
            hsh, ext = os.path.splitext(os.path.basename(wrk))
            job = os.path.splitext(wrk)[0] + ".job"
            shutil.move(wrk, job)
            cmd = "rm -rf {rdir}/{hsh}*".format(rdir=conf["rdir"], hsh=hsh)
            stdin, stdout, stderr = client.exec_command(cmd)
            err = "".join(stderr.readlines())
            if len(err) > 0:
                raise Exception("Could not remove remote files for %s.\nCommand: %s\nError: %s" % (hsh, cmd, err))

        ls = ftp.listdir()
        if len(ls) > 0:
            sys.stderr.write("warning: there is data left remotely (maybe remove manually):{list}\n".format(list="\n\t".join(ls)))

    elif args.action == "send":

        # check local upload folder for new job files
        # rename to wrk
        # transfer with sftp
        # enqueue with ssh
        # - mkdir of output dir
        # - actual command
        # - zip output dir
        # - remove output dir
        available = glob.glob("{ldir}/*.job".format(ldir=conf["ldir"]))
        available.sort(key=lambda x: os.path.getmtime(x))
        for job in available:
            # remove old errors
            with open(job, 'r') as jobf:
                jobd = json.load(jobf)
            if "error" in jobd:
                del jobd["error"]
                with open(job, "w") as jobf:
                    json.dump(jobd, jobf, indent=3, sort_keys=True)
            hsh, ext = os.path.splitext(os.path.basename(job))
            logging.debug("submitting %s" % hsh)

            wrk = os.path.splitext(job)[0] + ".wrk"
            shutil.move(job, wrk)
            ftp.put(wrk, conf["rdir"] + "/" + hsh + ".wrk")

            command = 'cd {rdir}; mkdir {hsh}; ' + conf["command"] + ' && zip -T -y -r {hsh}.zip {hsh}/ && rm -rf {hsh} && sleep 30'
            command = command.format(rdir=conf["rdir"], hsh=hsh)

            qcmd = 'cd {rdir}; echo `pwd`; ' + conf["queueing"]
            qcmd = qcmd.format(rdir=conf["rdir"], command=command, hsh=hsh)

            logging.debug("qcmd: %s" % (qcmd))

            stdin, stdout, stderr = client.exec_command(qcmd, get_pty=True)
            logging.debug("out: %s" % ("\n".join(stdout.readlines())))
            logging.debug("err: %s" % ("\n".join(stderr.readlines())))
            err = "\n".join(stderr.readlines())
            if len(err) > 0:
                raise Exception("Submission of %s failed.\nCommand: %s\nError: %s" % (hsh, qcmd, err))

    elif args.action == "recv":
        # check remote for results (zip & queue server output files)
        # transfer zip file
        # unzip
        # remove everything from remote
        # move wrk file to log
        ls = ftp.listdir()
        for z in [z for z in ls if z.endswith(".wrk")]:
            # get the hash
            hsh = os.path.splitext(z)[0]
            logging.debug("checking %s" % hsh)
            # check if a job with name hsh is still running (qstat -j hsh returns something on stdout)
            cmd = "{listqueue}".format(listqueue=conf["listqueue"])
            stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
            out = "".join(stdout.readlines())
            err = "".join(stderr.readlines())
    #         logging.debug("qstat\nstdout %s\nstderr%s\n" % (out, err))
            if len(err) > 0:
                raise Exception("Could not determine running jobs.\nCommand: %s\nError: %s" % (cmd, err))
            # check if the job is still in the queue
            # since qstat output is width limited only the first 9 chars are checked
            if hsh[:9] in out:
                continue

            logging.debug("getting %s" % hsh)
            # determine filenames
            wrk = "{ldir}/{hsh}.wrk".format(ldir=conf["ldir"], hsh=hsh)
            log = "{ldir}/{hsh}.log".format(ldir=conf["ldir"], hsh=hsh)

            logging.debug(wrk)
            if not os.path.exists(wrk):
                sys.stderr.write("No local work file for finished job %s, just removing it\n" % hsh)
                remove_from_remote(client, conf, hsh)
                continue

            # get job
            f = open(wrk, "r")
            job = json.load(f)
            f.close()

            # get results 
            results = [z for z in ls if z.startswith(hsh+".")]
            logging.debug("transfering %s" % str(results))
            try:
                for f in results:
                    ftp.get(conf["rdir"] + "/" + f, conf["ldir"] + "/" + f)
            except IOError:
                sys.stderr.write("Could transfer results from remote for %s. Will be retried next time.\n" % (hsh))

            # get queue server error file. if there is content save it to the error key in the job file
            errfile = [f for f in results if f.startswith(hsh + ".e")]
            if len(errfile) != 1:
                raise Exception("expected one error file, but found %d for %s" % (len(errfile), hsh))
            errfile = errfile[0]
            with open(conf["ldir"] + "/" + errfile) as f:
                errlines = f.readlines()
            if len(errlines) > 0:
                job["error"] = "".join(errlines)
                with open(wrk, "w") as f:
                    json.dump(job, f, indent=3, sort_keys=True)
                err = "{ldir}/{hsh}.err".format(ldir=conf["ldir"], hsh=hsh)
                shutil.move(wrk, err)
                sys.stderr.write("job {hsh} caused an error in mitos.\n{err} ".format(hsh=job["hsh"], err=job["error"]))
                if job["email"] != "None":
                    result_mail(job, conf)
                remove_from_remote(client, conf, hsh)
                continue

            # remove all temporary files of this job from the remote host
            remove_from_remote(client, conf, hsh)

            # (skipping the dir in case zip did not run [or zip file is not available yet])
            if "%s.zip" % hsh not in results:
                sys.stderr.write("No zip file found for finished job %s, waiting for next iteration\n" % (hsh))
                continue

            # unzip
            proc = subprocess.Popen("cd {ldir} && unzip -u -o -q {hsh}.zip".format(ldir=conf["ldir"], hsh=hsh), shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            stdout, stderr = proc.communicate()
            if proc.returncode != 0:
                with open(conf["ldir"] + '/' + "ziperror.txt", mode="a") as ziperror:
                    ziperror.write(hsh+"\n")
                raise ZIPError("Could not unzip: stdout (%s) stderr (%s)" % (stdout, stderr))

            # move wrk file to log file and remove (old) error info (from previous runs)
            shutil.move(wrk, log)

            oldumask = os.umask(0000)

            # change permission of everything (to enable webserver access)
            change_permissions("{ldir}/{hsh}/".format(ldir=conf["ldir"], hsh=hsh), 0777)
            change_permissions("{ldir}/{hsh}.zip".format(ldir=conf["ldir"], hsh=hsh), 0777)

            if len(errlines) == 0 and not args.flat:
                # move all files to subdirectories (HASH -> H/A/S/H)
                hdir = conf["ldir"] + "/" + "/".join(list(hsh[:-1]))
                lhsh = hsh[-1]
                logging.debug("hdir %s" % hdir)
                # create directories (may exist allready if job is restarted)
                try:
                    os.makedirs(hdir, 0777)
                except OSError as e:
                    if e.args[0] == 17:
                        pass
                    else:
                        os.umask(oldumask)
                        raise
                os.umask(oldumask)

                jobfiles = glob.glob("{ldir}/{hsh}*".format(ldir=conf["ldir"], hsh=hsh))
                for jf in jobfiles:
                    jfpfx, ext = os.path.splitext(jf)
                    logging.debug("%s -> %s" % (jf, hdir + "/" + lhsh + ext))
                    shutil.move(jf, hdir + "/" + lhsh + ext)
            # else:
            #     os.remove("{ldir}/{err}".format(ldir=conf["ldir"], err=errfile))

            if job["email"] != "None":
                result_mail(job, conf)

    elif args.action == "check":
        running = 0
        queued = 0
        error = ""

        day = datetime.timedelta(days=1)
        cmd = conf["listqueue"] + '-u mitosd -o "%.18i %.9P %.8j %.8u %.2t %.10M %.6D %R %V"'
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        err = "".join(stderr.readlines())
        if len(err) > 0:
            raise Exception("Could not determine running jobs.\nCommand: %s\nError: %s" % (cmd, err))

        lnr = 0
        for line in stdout.readlines():
            lnr += 1
            if lnr <= 2:
                continue

            line = line.split()
            logging.debug("%s" % (str(line)))

            # check job state: count r/q and report all others
            state = line[4]
            jid = line[0]
            name = line[2]

            if state == "R":
                running += 1
            elif state in ["PD", "RH", "RQ"]:
                queued += 1
#             elif state in ["Eqw"]:
#                 cmd = "{prefix}; qmod -cj {jid}".format(prefix=conf["prefix"], jid=jid)
#                 stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
#                 err = "".join(stderr.readlines())
#                 if len(err) > 0:
#                     raise Exception("Unable to fix state Eqw for %s.\nCommand: %s\nError: %s" % (jid, cmd, err))
#                 else:
#                     error += "Fixed state %s for job %s %s\n" % (state, jid, name)
            else:
                error += "Job %s %s in state %s\n" % (jid, name, state)

            startt = datetime.datetime.strptime(line[8], "%Y/%m/%dT%H:%M:%S")
            age = datetime.datetime.now() - startt
            if age > day:
                error += "Job %s %s older than 1 day\n" % (jid, name)
        if running + queued > conf["njobsthr"]:
            sys.stderr.write("More that %d jobs are in the queue (%d running, %d queued)\n" % (running + queued, running, queued))
        if error != "":
            sys.stderr.write("Queing error: \n%s" % error)

        ls = ftp.listdir_attr()
        for e in ls:
            age = datetime.datetime.now() - datetime.datetime.fromtimestamp(e.st_atime)
            if age > day:
                error += "File %s older than 1 day\n" % (e.filename)

    elif args.action == "stats":

        stats = {"date": None, "jobs": 0, "seqlen": 0, "jobs_day": 0, "jobs_week": 0, "jobs_month": 0, "queued": 0}

        stats["date"] = time.asctime(time.localtime(time.time()))
        if args.flat:
            files = glob.glob("{ldir}/*.log".format(ldir=conf["ldir"]))
        else:
            files = glob.glob("{ldir}/?/?/?/?/?/?/?/?.log".format(ldir=conf["ldir"]))
        for f in files:

            try:
                with open(f, "r") as fh:
                    job = json.load(fh)
            except ValueError, IOError:
                sys.stderr.write("stats warning: could not open/read {file}\n".format(file=f))
                continue

            if "seqlen" in job:
                stats["seqlen"] += job["seqlen"]
            stats["jobs"] += 1
            if (time.time() - os.path.getmtime(f)) <= 31 * 86400:
                stats["jobs_month"] += 1
            if (time.time() - os.path.getmtime(f)) <= 7 * 86400:
                stats["jobs_week"] += 1
            if (time.time() - os.path.getmtime(f)) <= 86400:
                stats["jobs_day"] += 1

        stats["queued"] += len(glob.glob("{ldir}/*.wrk".format(ldir=conf["ldir"])))
        stats["queued"] += len(glob.glob("{ldir}/*.job".format(ldir=conf["ldir"])))

        with open('{ldir}/stats.json'.format(ldir=conf["ldir"]), 'w') as fp:
            json.dump(stats, fp)
        change_permissions('{ldir}/stats.json'.format(ldir=conf["ldir"]), 0777)

    elif args.action == "clean":
        now = time.time()

#         matches = []
        for root, dirnames, filenames in os.walk(conf["ldir"]):
            # sys.stderr.write("r %s d %s f %s" % (root,dirnames, filenames))
            for filename in fnmatch.filter(filenames, '*.log'):
                fpath = os.path.join(root, filename)
                with open(fpath, "r") as f:
                    job = json.load(f)
                if job['hsh'] == "example":
                    continue
                basename = os.path.splitext(filename)[0]
                mtime = os.path.getmtime(os.path.join(root, filename))
                printdir = os.path.join(root, basename)[len(conf["ldir"]):]
                if (now - mtime) / 86400 < conf['maxage']:
                    continue
                cleaned = False
                # remove result directory
                if os.path.exists(os.path.join(root, basename)):
                    # sys.stderr.write("rm {path}\n".format(path=os.path.join(root, basename)))
                    cleaned = True
                    shutil.rmtree(os.path.join(root, basename))
                # remove all files except log file
                for f in glob.glob(os.path.join(root, basename) + "*"):
                    if f.endswith(".log"):
                        continue
                    # sys.stderr.write('rm {path}\n'.format(path=f))
                    cleaned = True
                    os.remove(f)
                if cleaned:
                    sys.stderr.write("cleaned {path} : {days} days old\n".format(path=printdir, days=int((now - mtime) / 86400)))

#                 matches.append(os.path.join(root, filename))
    else:
        Exception("unknown action %s" % args.action)

    # close sftp and ssh connection
    ftp.close()
    client.close()
